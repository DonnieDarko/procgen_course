class Cmd(object):
    def __init__(self, name, param):
        self.name = name
        self.param = param

class Rule(object):
    def match(self, cmd):
        pass
    def apply(self, cmd):
        pass

class RuleMgr(object):
    def __init__(self, rules):
        self.rules = rules

    def apply(self, start_cmds, nb):
        cmds = start_cmds
        for i in range(nb):
            cmds = self.apply_rules( cmds )
        return cmds

    def apply_rules(self, cmds):
        result = []
        for cmd in cmds:
            result.extend( self.apply_rule( cmd ) )
        return result

    def apply_rule(self, cmd):
        for rule in self.rules:
            if rule.match(cmd):
                return rule.apply(cmd)
        return [cmd]

class SimpleCmd(object):

    def match(self, cmd):
        return cmd.name in self.names

    def execute(self, cmd, context): 
        pass

class Renderer(object):
    def __init__(self, drawing_cmds):
        self.drawing_cmds = drawing_cmds

    def draw(self, commands):
        self.pre_draw()
        context = self.get_context()
        for cmd in commands:
            for dc in self.drawing_cmds:
                if dc.match(cmd):
                    dc.execute(cmd, context)
        self.post_draw()

    def pre_draw(self):
        pass

    def post_draw(self):
        pass

    def get_context(self):
        return {}
